from django.db import models

from wagtail.core.models import Page
from wagtail.core.fields import StreamField
from wagtail.admin.edit_handlers import FieldPanel, RichTextField
from wagtailstreamforms.blocks import WagtailFormBlock
from wagtail.core import blocks
from wagtail.core.blocks import DateBlock, DateTimeBlock, URLBlock, EmailBlock, TimeBlock, StreamBlock, ChoiceBlock
from wagtail.core.blocks import DateTimeBlock, TimeBlock, BlockQuoteBlock, PageChooserBlock, ListBlock, BooleanBlock
from wagtail.documents.blocks import DocumentChooserBlock
from wagtail.embeds.blocks import EmbedBlock
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel, InlinePanel
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.contrib.table_block.blocks import TableBlock
from wagtail.search import index

from streams import block
from .validators import validate_blocked_text

# 
class ExamplePage(Page):
    """ExamplePage page class"""

    template = "example/example_page.html"
    subpage_types = ['example.ExamplePage']
    parent_page_types = [
        'example.ExamplePage',
        'home.HomePage',
    ] 

    content = StreamField(
        [
        ('rich_text_sample', blocks.RichTextBlock()),
        ('image', ImageChooserBlock()),
        ('gallery', blocks.StreamBlock(
                [
                    ('image', blocks.StructBlock([
                        ('new_image', ImageChooserBlock(required=False)),
                    ]),),
                ]
            )),
        ('structural_block_section', blocks.StructBlock([
            ('title', block.TitleBlock()),
            ('rich_text', block.SimpleRichtextBlock()),  
            ('buttonA', block.ButtonBlock()),
            ('buttonB', block.ButtonBlock()),
        ]),
        ),
        ], blank=True
    )

    content_panels = Page.content_panels + [
        StreamFieldPanel("content"),
    ]

    class Meta:  
        verbose_name = "Example Page"
        verbose_name_plural = "Example Pages"

