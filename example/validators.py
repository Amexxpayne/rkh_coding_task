from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_blocked_text(value):
    if "the" in value.lower().split():
        raise ValidationError(
            _('%(value)s cannot be saved, remove the word "the" and try again'),
            params={'value': value},
        )


def validate_blocked_rich_text(value):
    title_value = ["de"]#title_value.split() # convert to a list made from each words 
    print(title_value)
    print(value)
    for val in title_value:
        if val in value:
            raise ValidationError(
                _('%(value)s cannot be saved, remove the word '+ val +' and try again'),
                params={'value': value},
            )