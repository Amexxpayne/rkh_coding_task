
from wagtail.core import blocks
from wagtail.core.templatetags.wagtailcore_tags import richtext
from wagtail.images.blocks import ImageChooserBlock

from example.validators import *


class TitleBlock(blocks.StructBlock):
    """Title and text and nothing else."""

    title = blocks.CharBlock(required=True, help_text="Add your title", validators=[validate_blocked_text])

    class Meta:  # nl
        template = "streams/title_and_text_block.html"
        icon = "edit"
        label = "Title"


class SimpleRichtextBlock(blocks.RichTextBlock):
    """Richtext without limited features."""

    def __init__(
        self, required=True, help_text=None, editor="default", features=None, **kwargs
    ):  # noqa
        super().__init__(**kwargs)
        self.features = ["bold", "p", "ol"]
                
    class Meta:  ##
        template = "streams/richtext_block.html"
        icon = "edit"
        label = ""


class LinkStructValue(blocks.StructValue):
    def url(self):
        button_page = self.get('button_page')
        button_url = self.get('button_url')
        if button_page:
            return button_page.url
        elif button_url:
            return button_url

        return None


class ButtonBlock(blocks.StructBlock):
    button_page = blocks.PageChooserBlock(required=False, help_text='If selected, this url will be used first')
    button_url = blocks.URLBlock(required=False, help_text='If added, this url will be used secondarily to the button page')

    class Meta:  
        template = "streams/button_block.html"
        icon = "placeholder"
        label = "Single Button"
        value_class = LinkStructValue




 